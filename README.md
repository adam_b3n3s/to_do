# TO-DO

This is very simple terminal to do application made in python.

Dates are always in this format: yyyy-mm-dd

## Usage

- add "name" "date" -- add to_do with given name and date
- remove "name" -- remove to_do with given name
- list -- list all the to_do
- sort -- sort all the to_do by date
- today -- print todays to_do
- tomorrow -- print tomorrow to_do
- remove all -- remove all the to_do
- done "name" -- make to_do with given name as done
- list done -- list all done to_do
- rename "what" "to" -- rename this to_do
- redate "what" "date" -- redate this to_do

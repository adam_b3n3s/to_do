#!/usr/bin/env python3

import datetime
import sys

def main():
    if (len(sys.argv) > 1):
        if sys.argv[1] == "-help":
            print("Have dates always in this format: yyyy-mm-dd")
            print()
            print("What you can do with ./to_do:")
            print("     add \"name\" \"date\" -- add to_do with given name and date")
            print("     remove \"name\" -- remove to_do with given name")
            print("     list -- list all the to_do")
            print("     sort -- sort all the to_do by date")
            print("     today -- print todays to_do")
            print("     tomorrow -- print tomorrow to_do")
            print("     remove all -- remove all the to_do")
            print("     done \"name\" -- make to_do with given name as done")
            print("     list done -- list all done to_do")
            print("     rename \"what\" \"to\" -- rename this to_do")
            print("     redate \"what\" \"date\" -- redate this to_do")
        try:
            if sys.argv[1] == "add":
                is_there = False
                invalid_date = False
                what = sys.argv[2]
                date = sys.argv[3]
                try: 
                    datetime.datetime.strptime(date, "%Y-%m-%d").strftime("%Y-%m-%d")
                except: 
                    invalid_date = True
                    print("Invalid date format. Valid format: yyyy-mm-dd", file=sys.stderr)
                if invalid_date == False:
                    file = open("/home/adam/to_do/.to_do.txt", "r")
                    for line in file:
                        line = line.split()
                        

                        line_without_date = " ".join(line[:-1])
                        if line_without_date == what:
                            print("This item with this name is already there", file=sys.stderr)
                            is_there = True
                    file.close()
                    if is_there == False:
                        file = open("/home/adam/to_do/.to_do.txt", "a")
                        file.write(what + " " + date +'\n')
                        file.close()
        except:
            print("Invalid format. Valid format: ./to_do add \"name\" \"date\"", file=sys.stderr)
            exit(1)

        try:
            if sys.argv[1] == "remove":
                what = sys.argv[2]
                with open("/home/adam/to_do/.to_do.txt", 'r') as file:
                    lines = file.readlines()

                for line in lines:
                    line_without_date = line[:-12]
                    if line_without_date == what:
                        lines.remove(line)

                with open("/home/adam/to_do/.to_do.txt", 'w') as file:
                    file.writelines(lines)
        except:
            print("Invalid format. Valid format: ./to_do remove \"name\"", file=sys.stderr)
            exit(1)
        if (len(sys.argv) == 2):
            if sys.argv[1] == "list":
                with open("/home/adam/to_do/.to_do.txt", 'r') as file:
                    for line in file:
                        today = datetime.datetime.today().strftime("%Y-%m-%d")
                        if today > line.split()[-1]:
                            print("\033[31m{}\033[0m".format(line), end='')
                        else:
                            print(line, end='')

        elif sys.argv[1] == "list" and sys.argv[2] != "done":
            with open("/home/adam/to_do/.to_do.txt", 'r') as file:
                for line in file:
                    print(line, end='')
        if sys.argv[1] == "sort":
            modified_lines = []
            with open("/home/adam/to_do/.to_do.txt", 'r') as file:
                for line in file:
                    modified_lines.append(line.split())
            modified_lines.sort(key=lambda x: x[-1])
            for i in range(modified_lines.__len__()):
                modified_lines[i] = " ".join(modified_lines[i])
            with open("/home/adam/to_do/.to_do.txt", 'w') as file:
                for i in modified_lines:
                    file.write(i+'\n')
        if sys.argv[1] == "today":
            this_day = []
            today = datetime.datetime.today().strftime("%Y-%m-%d")
            print("For informative reasons, this day is: " + today)
            with open("/home/adam/to_do/.to_do.txt", 'r') as file:
                for line in file:
                    if today == line.split()[-1]:
                        this_day.append(line)
            if this_day.__len__() == 0:
                print("No to_do today")
            else:
                for i in this_day:
                    print(i, end='')
        if sys.argv[1] == "tomorrow":
            this_day = []
            tomorrow = datetime.datetime.today().strftime("%Y-%m-%d")
            tomorrow = str(datetime.datetime.strptime(tomorrow, "%Y-%m-%d") + datetime.timedelta(days=1))
            tomorrow = tomorrow.split()[0]
            print("For informative reasons, tomorrow day is: " + tomorrow)
            with open("/home/adam/to_do/.to_do.txt", 'r') as file:
                for line in file:
                    if tomorrow == line.split()[-1]:
                        this_day.append(line)
            if this_day.__len__() == 0:
                print("No to_do tomorrow")
            else:
                for i in this_day:
                    print(i, end='')
        if sys.argv[1] == "remove" and sys.argv[2] == "all":
            print("Are you sure you want to remove all to_do? (y/n)")
            if input() == "y":
                file = open("/home/adam/to_do/.to_do.txt", "w")
                file.write("")
                file.close()
                print("All to_do removed")
        try:
            if sys.argv[1] == "done":
                what = sys.argv[2]
                with open("/home/adam/to_do/.to_do.txt", 'r') as file:
                    lines = file.readlines()

                for line in lines:
                    line_without_date = line[:-12]
                    if line_without_date == what:
                        lines.remove(line)
                        removed = line
                if removed:
                    with open("/home/adam/to_do/.done_to_do.txt", 'a') as file:
                        file.write(removed)


                with open("/home/adam/to_do/.to_do.txt", 'w') as file:
                    file.writelines(lines)
        except:
            print("Invalid format. Valid format: ./to_do done \"name\"", file=sys.stderr)
            exit(1)
        if (len(sys.argv) > 2):
            if sys.argv[1] == "list" and sys.argv[2] == "done":
                with open("/home/adam/to_do/.done_to_do.txt", 'r') as file:
                    for line in file:
                        print(line, end='')
        if sys.argv[1] == "rename":
            what = sys.argv[2]
            to = sys.argv[3]
            with open("/home/adam/to_do/.to_do.txt", 'r') as file:
                files = file.readlines()
            file_insert = ""
            for line in files:
                line = line.split()
                if line[0:-1] == what.split():
                    print("Renaming " + what + " to " + to + "..." + "are you sure? (y/n)")
                    if input() == "y":
                        line[0:-1] = to.split()
                line = " ".join(line)
                file_insert += line + "\n"
            with open("/home/adam/to_do/.to_do.txt", 'w') as file:
                file.write(file_insert)
        
        if sys.argv[1] == "redate":
            what = sys.argv[2]
            date = sys.argv[3]
            invalid_date = False
            try: 
                datetime.datetime.strptime(date, "%Y-%m-%d").strftime("%Y-%m-%d")
            except: 
                invalid_date = True
                print("Invalid date format. Valid format: yyyy-mm-dd", file=sys.stderr)



            if invalid_date == False:
                with open("/home/adam/to_do/.to_do.txt", 'r') as file:
                    files = file.readlines()
                file_insert = ""
                for line in files:
                    line = line.split()
                    if line[0:-1] == what.split():
                        print("Redating " + what + " from " + line[-1] + " to " +  date + "..." + "are you sure? (y/n)")
                        if input() == "y":
                            line[-1] = date
                    line = " ".join(line)
                    file_insert += line + "\n"
                with open("/home/adam/to_do/.to_do.txt", 'w') as file:
                    file.write(file_insert)
            

    else:
        print("Welcome to to_do. Type ./to_do.py -help for help.")
    



if __name__ == '__main__':
    main()

